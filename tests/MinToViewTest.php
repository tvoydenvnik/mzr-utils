<?php

namespace MZR\Utils\Tests;


use MZR\Utils\MinToView;
use PHPUnit\Framework\TestCase;


class MinToViewTest extends TestCase
{

    public function testMinToView()
    {

        $this->assertEquals([
            "hours" => 0,
            "mins" => 5
        ], MinToView::minToView(5));

        $this->assertEquals([
            "hours" => 1,
            "mins" => 5
        ], MinToView::minToView(65));

        $this->assertEquals([
            "hours" => 2,
            "mins" => 6
        ], MinToView::minToView(126));
    }

    public function testMinToViewS()
    {

        $this->assertEquals("5 мин", MinToView::minToViewS(5));

        $this->assertEquals("1 ч 5 мин", MinToView::minToViewS(65));

        $this->assertEquals( "2 ч 6 мин", MinToView::minToViewS(126));
    }


}
