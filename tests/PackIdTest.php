<?php

namespace MZR\Utils\Tests;


use MZR\Utils\PackId;
use PHPUnit\Framework\TestCase;


class PackIdTest extends TestCase
{

    public function testPackId()
    {
        $this->assertEquals("10001", PackId::packId(1, 100));
        $this->assertEquals("30202", PackId::packId(2, 302));
        $this->assertEquals("3340202", PackId::packId(2, 33402));
    }

    public function testExtractSourceId()
    {
        $this->assertEquals(1, PackId::extractSourceId("10001"));
        $this->assertEquals(2, PackId::extractSourceId("30202"));
    }

    public function testExtractLocalId()
    {
        $this->assertEquals(100, PackId::extractLocalId("10001"));
        $this->assertEquals(302, PackId::extractLocalId("30202"));
    }

}
