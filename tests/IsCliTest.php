<?php

namespace MZR\Utils\Tests;

use MZR\Utils\IsCLI;
use PHPUnit\Framework\TestCase;



class IsCliTest extends TestCase
{

    public function testIsCli()
    {
        $this->assertTrue(IsCLI::isCli());
    }

    public function testIsCliFalse()
    {

        global $_SERVER;
        if(!is_array($_SERVER)){
            $_SERVER = [];
        }
        $_SERVER["REMOTE_ADDR"] = "test";
        $_SERVER["HTTP_USER_AGENT"] = "test";
        $this->assertTrue(IsCLI::isCli());
    }


}
