<?php

namespace MZR\Utils\Tests;

use MZR\Utils\Fix1cJson;
use PHPUnit\Framework\TestCase;


class Fix1cJsonTest extends TestCase
{

    public function testFix()
    {

        $text = 'ï»¿{"id":1438,"nutrie';


        $this->assertEquals('{"id":1438,"nutrie', Fix1cJson::fix($text));


    }
}
