<?php


namespace MZR\Utils\Tests;

use MZR\Utils\ArrayUtils;
use PHPUnit\Framework\TestCase;


class ArrayUtilsTest extends TestCase
{

    public function testGetNumericUniqueArray()
    {
        $this->assertEquals([1, 2, 3], ArrayUtils::getNumericUniqueArray([0, 0, 1, 1, 1, 2, 3, 3, 3]));
    }

    public function testNumericUniqueArrayMayHasZero()
    {
        $this->assertEquals([0, 1, 2, 3], ArrayUtils::getNumericUniqueArrayMayHasZero([0, 0, 1, 1, 1, 2, 3, 3, 3]));
    }

    public function testToStringEmptyCanNull()
    {
        $this->assertEquals(null, ArrayUtils::toString([]));
        $this->assertEquals(null, ArrayUtils::toString(""));
        $this->assertEquals(null, ArrayUtils::toString(null));
    }

    public function testToStringEmptyNotCanNull()
    {
        $this->assertEquals("", ArrayUtils::toString([], false));
        $this->assertEquals("", ArrayUtils::toString("", false));
        $this->assertEquals("", ArrayUtils::toString(null, false));
    }

    public function testToStringEmpty()
    {
        $this->assertEquals("[1,2,3]", ArrayUtils::toString([1, 2, 3], false));
    }

    public function testRandomValues()
    {

        $rawArray = [1, 2, 3, 4, 5, 6];
        $rand = ArrayUtils::randomValues($rawArray, 5);
        $this->assertEquals(5, count($rand));
        $this->assertNotEquals($rawArray, $rand);
    }
}
