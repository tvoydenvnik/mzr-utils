<?php

namespace MZR\Utils\Tests;

use MZR\Utils\DateUtils;
use PHPUnit\Framework\TestCase;


class DateUtilsTest extends TestCase
{

    public function testLastDayOfYear()
    {
        $this->assertEquals(new \DateTime("2020-12-31"), DateUtils::lastDayOf("year", new \DateTime("2020-01-01")));
    }

    public function testLastDayOfQuarter()
    {
        $this->assertEquals(new \DateTime("2020-03-31"), DateUtils::lastDayOf("quarter", new \DateTime("2020-01-01")));
    }

    public function testLastDayOfMonth()
    {
        $this->assertEquals(new \DateTime("2020-01-31"), DateUtils::lastDayOf("month", new \DateTime("2020-01-01")));
    }

    public function testLastDayOfWeek()
    {
        $this->assertEquals(new \DateTime("2020-01-05"), DateUtils::lastDayOf("week", new \DateTime("2020-01-02")));
    }

    public function testFirstDayOfYear()
    {
        $this->assertEquals(new \DateTime("2020-01-01"), DateUtils::firstDayOf("year", new \DateTime("2020-05-01")));
    }

    public function testFirstDayOfQuarter()
    {
        $this->assertEquals(new \DateTime("2020-01-01"), DateUtils::firstDayOf("quarter", new \DateTime("2020-03-01")));
    }

    public function testFirstDayOfMonth()
    {
        $this->assertEquals(new \DateTime("2020-01-01"), DateUtils::firstDayOf("month", new \DateTime("2020-01-22")));
    }

    public function testFirstDayOfWeek()
    {
        $this->assertEquals(new \DateTime("2019-12-30"), DateUtils::firstDayOf("week", new \DateTime("2020-01-04")));
    }
}
