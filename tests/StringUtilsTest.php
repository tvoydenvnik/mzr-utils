<?php

namespace MZR\Utils\Tests;


use MZR\Utils\StringUtils;
use PHPUnit\Framework\TestCase;


class StringUtilsTest extends TestCase
{

    public function testStartsWith()
    {
        $this->assertTrue(StringUtils::startsWith("start", "st"));
        $this->assertFalse(StringUtils::startsWith("start", "rt"));
    }

    public function testEndsWith()
    {
        $this->assertFalse(StringUtils::endsWith("start", "st"));
        $this->assertTrue(StringUtils::endsWith("start", "rt"));
    }

    public function testStrrpos()
    {
        $this->assertEquals(strrpos("start", "st"), StringUtils::strrpos("start", "st"));
        $this->assertEquals(strrpos("start", "rt"), StringUtils::strrpos("start", "rt"));
    }

    public function testStrpos()
    {
        $this->assertEquals(strpos("start", "st"), StringUtils::strpos("start", "st"));
        $this->assertEquals(strpos("start", "rt"), StringUtils::strpos("start", "rt"));
    }

    public function testStrlen()
    {
        $this->assertEquals(strlen("start"), StringUtils::strlen("start"));
    }

    public function testSubstr()
    {
        $this->assertEquals(substr("apple", 1, 2), StringUtils::substr("apple", 1, 2));
        $this->assertEquals("pp", StringUtils::substr("apple", 1, 2));
        $this->assertEquals("бл", StringUtils::substr("яблоко", 1, 2));
        //$this->assertEquals(substr("яблоко", 1, 2), StringUtils::substr("яблоко", 1, 2));
    }

    public function testStrtoupper()
    {
        $this->assertEquals(strtoupper("apple"), StringUtils::strtoupper("apple"));
        $this->assertEquals("APPLE", StringUtils::strtoupper("apple"));
        //$this->assertEquals(strtoupper("яблоко"), StringUtils::strtoupper("яблоко"));
        $this->assertEquals("ЯБЛОКО", StringUtils::strtoupper("яблоко"));
    }

    public function testStrtolower()
    {
        $this->assertEquals(strtolower("APPLE"), StringUtils::strtolower("APPLE"));
        $this->assertEquals("apple", StringUtils::strtolower("APPLE"));
        //$this->assertEquals(strtolower("яблоко"), StringUtils::strtolower("яблоко"));
        $this->assertEquals("яблоко", StringUtils::strtolower("ЯБЛОКО"));
    }

    public function testCcfirstUtf8()
    {
        $this->assertEquals("Apple", StringUtils::ucfirst_utf8("apple"));
        $this->assertEquals("Яблоко", StringUtils::ucfirst_utf8("яблоко"));
    }

    public function tesTruncate()
    {
        $this->assertEquals("яблоко", StringUtils::truncate("яблоко apple", 6));
    }


}
