<?php

namespace MZR\Utils\Tests;

use MZR\Utils\JSON;
use PHPUnit\Framework\TestCase;


class JSONTest extends TestCase
{

    public function testFileGetContents()
    {
        $this->assertEquals("{\"id\":1438,\"nutrie", JSON::file_get_contents(__DIR__ . "/fix1C.txt"));
    }

    public function testJsonFromFile()
    {
        $this->assertEquals(false, JSON::json_from_file(__DIR__ . "/xxxxx.txt"));
        $this->assertEquals(["test" => 1], JSON::json_from_file(__DIR__ . "/textJson.json"));
    }

    public function testJsonEncode()
    {
        $this->assertEquals('{"ru":"Русские символы + english"}', JSON::json_encode(["ru" => "Русские символы + english"]));
    }

    public function testJsonDecode()
    {
        $this->assertEquals(["ru" => "Русские символы + english"], JSON::json_decode('{"ru":"Русские символы + english"}'));
    }

    public function testJsonDecodeArray()
    {
        $this->assertEquals(["ru" => "test"], JSON::json_decode(["ru" => "test"]));
    }

    public function testJsonDecodeEmpty()
    {
        $this->assertEquals([], JSON::json_decode(""));
        $this->assertEquals([], JSON::json_decode(null));
    }

    public function testJsonDecodeError()
    {
        $this->assertEquals(null, JSON::json_decode("[", true, true));
        $this->assertEquals(null, JSON::json_decode("[sssssssssssssssssss"));
        $this->assertEquals([], JSON::json_decode(new \DateTime("")));
        $this->assertEquals(null, JSON::json_decode(new \DateTime(""), true, true));
    }

}
