<?php

namespace MZR\Utils;


class Fix1cJson
{
    /*
    * fix
    * По какой то причине появляется символ 239 в самом начале файла, при выгрузке файлов из 1с
    * Которой не дает работать функции json_decode
     * update 2017-05-29: 1c добавляет BOM (EF BB BF) https://1c-programmer-blog.ru/raznoe/udalenie-bom-ef-bb-bf-v-1s.html
    */
    public static function fix($content)
    {
        //        if(ord($content) == 239){
        //            $content = substr($content, 1);
        //        }
        $count = 0;
        $content = trim($content);
        $contentFix = $content;
        while (StringUtils::substr($contentFix, 0, 1) != '{' && StringUtils::substr($contentFix, 0, 1) != '[') {
            $contentFix = StringUtils::substr($contentFix, 1);
            $count = $count + 1;
            if ($count > 10) {
                break;
            }
        }

        return $contentFix;
    }
}
