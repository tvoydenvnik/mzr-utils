<?php


namespace MZR\Utils;

class PackId
{


    public static function packId($nIdSource, $nLocalId, $base = 100)
    {
        return $nLocalId * $base + $nIdSource;
    }

    public static function extractSourceId($id, $base = 100)
    {
        return $id % $base;
    }

    public static function extractLocalId($id, $base = 100)
    {
        return (int)floor($id / $base);
    }


}
