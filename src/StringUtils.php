<?php

namespace MZR\Utils;


class StringUtils
{
    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (self::substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (self::substr($haystack, -$length) === $needle);
    }

    private static $_mbstringLoaded = null;

    public static function mbstringLoaded()
    {

        if (is_null(self::$_mbstringLoaded)) {
            self::$_mbstringLoaded = extension_loaded('mbstring');
        }
        return self::$_mbstringLoaded;
    }


    //http://php.net/manual/ru/mbstring.overload.php
    //not exists 	stripos
    //not exists 	strripos
    //not exists 	strstr
    //not exists 	stristr
    //not exists 	strrchr
    //not exists 	substr_count

    public static function strrpos($haystack, $needle, $offset = 0, $encoding = 'utf-8')
    {

        if (self::mbstringLoaded()) {
            return mb_strrpos($haystack, $needle, $offset, $encoding);
        } else {
            return strrpos($haystack, $needle, $offset);
        }
    }

    public static function strpos($haystack, $needle, $offset = 0, $encoding = 'utf-8')
    {

        if (self::mbstringLoaded()) {
            return mb_strpos($haystack, $needle, $offset, $encoding);
        } else {
            return strpos($haystack, $needle, $offset);
        }
    }

    public static function strlen($string, $encoding = 'utf-8')
    {

        if (self::mbstringLoaded()) {
            return mb_strlen($string, $encoding);
        } else {
            return strlen($string);
        }
    }

    public static function substr($string, $start, $length = null, $encoding = 'utf-8')
    {

        if (self::mbstringLoaded()) {
            return mb_substr($string, $start, $length, $encoding);
        } else {
            return substr($string, $start, $length);
        }
    }

    public static function strtoupper($str, $encoding = 'utf-8')
    {

        if (self::mbstringLoaded()) {
            return mb_strtoupper($str, $encoding);
        } else {
            return strtoupper($str);
        }
    }

    //done 2017-06-26
    public static function strtolower($str, $encoding = 'utf-8')
    {

        if (self::mbstringLoaded()) {
            return mb_strtolower($str, $encoding);
        } else {
            return strtolower($str);
        }
    }


    public static function ucfirst_utf8($string, $e = 'utf-8')
    {
        if (function_exists('mb_strtoupper')) {
            $string = mb_strtoupper(mb_substr($string, 0, 1, $e), $e) . mb_substr($string, 1, mb_strlen($string, $e), $e);
        } else {
            $string = ucfirst($string);
        }
        return $string;
    }

    public static function truncate($text, $limit, $postFix = '...')
    {
        if ($limit == 0) {
            return $text;
        }
        if (StringUtils::strlen($text) > $limit) {
            return StringUtils::substr($text, 0, $limit) . $postFix;
        } else {
            return $text;
        }
    }
}
