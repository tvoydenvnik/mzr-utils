<?php

namespace MZR\Utils;


class ArrayUtils
{


    public static function getNumericUniqueArray($arArray)
    {

        if (is_array($arArray) == false) {
            return array();
        }
        $arArrayNew = array();
        foreach ($arArray as $key => $val) {
            if (intval($val) > 0) {
                array_push($arArrayNew, intval($val));
            }

        }
        return array_values(array_unique($arArrayNew));

    }

    public static function getNumericUniqueArrayMayHasZero($arArray)
    {

        if (is_array($arArray) == false) {
            return array();
        }
        $arArrayNew = array();
        foreach ($arArray as $key => $val) {
            if (intval($val) >= 0) {
                array_push($arArrayNew, intval($val));
            }

        }
        return array_values(array_unique($arArrayNew));

    }


    public static function toString($pValue, $canNull = true)
    {
        if (is_array($pValue)) {
            if ($canNull == true && count($pValue) == 0) {
                return null;
            }
            return JSON::json_encode($pValue);
        }

        if ($canNull == true && $pValue == '') {
            return null;
        }

        if ($canNull == false && is_null($pValue)) {
            return '';
        }

        return $pValue;
    }

    public static function randomValues($array, $countOfValues)
    {
        if (is_array($array) == false) {
            return [];
        }
        if ($countOfValues > count($array)) {
            $countOfValues = count($array);
        }

        if ($countOfValues == 0) {
            return [];
        }

        $arResult = [];
        $rand = array_rand($array, $countOfValues);

        if (is_array($rand)) {
            foreach ($rand as $index) {
                array_push($arResult, $array[$index]);
            }
        } elseif (is_int($rand)) {
            array_push($arResult, $array[$rand]);
        }

        return $arResult;
    }
}
