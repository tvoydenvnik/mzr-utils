<?php

namespace MZR\Utils;


class MinToView
{

    /**
     * Преобразует минуты в Часы + минуты
     * @returns {{hours: number, mins: number}}
     */
    public static function minToView($nValue)
    {
        $nValue = intval($nValue);

        $hours = floor($nValue / 60);
        $min = $nValue - $hours * 60;

        return
            array(
                "hours" => $hours,
                "mins" => $min
            );
    }

    public static function minToViewS($nValue)
    {
        $hm2 = self::minToView($nValue);
        if ($hm2["hours"] == 0 && $hm2["mins"] == 0) {
            return '';
        } else if ($hm2["hours"] == 0) {
            return '' . $hm2["mins"] . ' мин';
        } else if ($hm2["mins"] == 0) {
            return '' . $hm2["hours"] . ' ч';
        } else {
            return '' . $hm2["hours"] . ' ч ' . $hm2["mins"] . ' мин';
        }
    }

}
